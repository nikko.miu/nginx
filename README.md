# NGINX Docker Build

This is a Docker build of NGINX. It uses a custom server header tag `awesome-apricot`. It has been built without having a configuration file at `/etc/nginx/nginx.conf`.

To run this container you will need to either build from this image and `COPY` a new configuration file. Or you will need to mount a volume for the location of the configuration.

## Image Tags

Image tags correspond to the branches that the images were built on.

**Note**: There is no Docker image tagged with `latest`

The production Docker image is:

```
registry.gitlab.com/nikko.miu/nginx:master
```

The staging Docker image is:

```
registry.gitlab.com/nikko.miu/nginx:develop
```

## Configuration

Add configuration to the `/config` directory to have them copied to the
`/etc/nginx/` directory.
