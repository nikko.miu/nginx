FROM alpine

MAINTAINER Nikko Miu <nikkoamiu@gmail.com>

RUN apk add --no-cache bash curl

RUN curl https://gist.githubusercontent.com/nikkomiu/cd15d615fc3390ebbd79e9d078458d10/raw/nginx_install.sh | bash -s 1.17.6

RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80 443

VOLUME ["/etc/nginx"]

CMD ["nginx", "-g", "daemon off;"]
